/******************************************************************************
 * Project name: MyNote
 * Package: application.notes.view
 * File: Note.java
 * Date:  11.09.2016
 * Last modify: 11.09.2016
 * Author: Jakub Horák
 *
 * Description: Fill the tableView
 *        
 ******************************************************************************/

/**
 * @file ScheduleOverviewController.java
 * 
 * @brief Controller.
 */

package application.notes.view;

import application.notes.model.Note;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;

/**
*
* @brief Controller of tableView in ScheduleOverview.fxml.
*/
public class ScheduleOverviewController {
	@FXML
	private TableView<Note> notesView;
	@FXML
	private TableColumn<Note, String> endDateColumn;
	@FXML
	private TableColumn<Note, String> titleColumn;
	@FXML
	private TableColumn<Note, String> textColumn;
	
	public ScheduleOverviewController() {}
	
	/**
	 * Initializes controll class
	 */
	@FXML
	private void initialize() {
		titleColumn.setCellValueFactory(cellData -> cellData.getValue().titleProperty());
		textColumn.setCellValueFactory(cellData -> cellData.getValue().textProperty());
	}
	
	/**
	 * Add observable list to the table.
	 * 
	 * @param notes	Notes of the active notepad.
	 */
	public void addDataToTable(ObservableList<Note> notes) {
        notesView.setItems(notes);
	}
}
