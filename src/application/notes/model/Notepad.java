/******************************************************************************
 * Project name: MyNote
 * Package: application.notes.data
 * File: Notepad.java
 * Date:  08.09.2016
 * Last modify: 09.09.2016
 * Author: Jakub Horák
 *
 * Description: Class represents notepads, which obtains self notes.
 *        
 ******************************************************************************/

/**
 * @file Notepad.java
 * 
 * @brief Class Notepad - Create notepads, add notes and return list of notes
 */

package application.notes.model;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @brief Class creates notepad and manages its content.
 */
public class Notepad {
	
	private final StringProperty title;
	// List of notes in notepad.
	private ObservableList<Note> notes = FXCollections.observableArrayList();
	
	// List of notepads in app
	private static ObservableList<Notepad> notepads = FXCollections.observableArrayList();
	
	
	/**
	 * Create new notepad with required title.
	 * 
	 * @param title Required title of notepad.
	 */
	public Notepad(String title) {
		this.title = new SimpleStringProperty(title);
	}
	
	/**
	 * Add notepad to list of all notepads.
	 * @param notepad
	 */
	public static void addNotepad(Notepad notepad) {
		notepads.add(notepad);
	}
	
	/**
	 * Add note to notepad.
	 * 
	 * @param note Note to add to notepad.
	 */
	public void addNote(Note note) {
		notes.add(note);
	}
	
	/**
	 * Get all notes from notepad.
	 * 
	 * @return List of notes.
	 */
	public ObservableList<Note> getNotes() {
		return notes;
	}
	
	/**
	 * Get list of notepads.
	 * @return
	 */
	public static ObservableList<Notepad> getNotepads() {
		return notepads;
	}

	public final StringProperty titleProperty() {
		return this.title;
	}
	

	public final String getTitle() {
		return this.titleProperty().get();
	}
	

	public final void setTitle(final String title) {
		this.titleProperty().set(title);
	}
}
/*** End of file Notepad.java ***/
