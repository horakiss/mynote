/******************************************************************************
 * Project name: MyNote
 * Package: application.notes.data
 * File: Note.java
 * Date:  08.09.2016
 * Last modify: 09.09.2016
 * Author: Jakub Horák
 *
 * Description: 
 *        
 ******************************************************************************/

/**
 * @file Note.java
 * 
 * @brief Class
 */

package application.notes.model;

import java.time.LocalDate;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
*
* @brief Class creates note.
*/
public class Note{
	
	private final StringProperty title;
	private final StringProperty text;
	//private final ObjectProperty<LocalDate> inserted;
	//private final ObjectProperty<LocalDate> end;
	private final IntegerProperty priority;
	
	/**
	 * Contructor creates new notes without date's details.
	 * 
	 * @param title		Title of note.
	 * @param text		Description of note.
	 * @param priority	Priority of day (Integer 0-10).
	 */
	private Note(String title, String text, Integer priority) {
		this.title = new SimpleStringProperty(title);
		this.text = new SimpleStringProperty(text);
		this.priority = new SimpleIntegerProperty(priority);
	}
	
	/**
	 * Create new note without date details.
	 * 
	 * @param title		Title of note.
	 * @param text		Description of note.
	 * @param priority	Priority of day (Integer 0-10).
	 * 
	 * @return New note.
	 */
	public static Note createNote(String title, String text, Integer priority) {
		return createNote(title, text, null, null, priority);
	}
	
	/**
	 * Create new note.
	 * 
	 * @param title		Title of note.
	 * @param text		Description of note.
	 * @param start		Creation date of note.
	 * @param end		Deadline.
	 * @param priority	Priority of day (Integer 0-10).
	 * 
	 * @return New note.
	 */
	public static Note createNote(String title, String text, LocalDate start, LocalDate end, Integer priority) {
		if(title == null || title == "") {
			return null;			
		}
		
		if(priority < 0 || priority > 10) {
			priority = 0;
		}
		
		return new Note(title,text,priority);
	}

	public final StringProperty titleProperty() {
		return this.title;
	}
	
	public final String getTitle() {
		return this.titleProperty().get();
	}
	
	public final void setTitle(final String title) {
		this.titleProperty().set(title);
	}
	
	public final StringProperty textProperty() {
		return this.text;
	}
	
	public final String getText() {
		return this.textProperty().get();
	}
	
	public final void setText(final String text) {
		this.textProperty().set(text);
	}
	
	public final IntegerProperty priorityProperty() {
		return this.priority;
	}
	
	public final int getPriority() {
		return this.priorityProperty().get();
	}
	
	public final void setPriority(final int priority) {
		this.priorityProperty().set(priority);
	}	
}
/*** End of file Note.java ***/
