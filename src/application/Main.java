/******************************************************************************
 * Project name: MyNote
 * Package: application
 * File: Main.java
 * Date:  08.09.2016
 * Last modify: 11.09.2016
 * Author: Jakub Horák
 *
 * Description: Main class.
 *        
 ******************************************************************************/

/**
 * @file Main.java
 * 
 * @brief Main class - Initialize layouts, scenes.
 */

package application;
	
import application.notes.model.Note;
import application.notes.model.Notepad;
import application.notes.view.ScheduleOverviewController;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;

/**
*
* @brief Main class loads FXML files. Set layouts, scenes and stages.
*/
public class Main extends Application {
	
	private Stage primaryStage;
	private BorderPane rootLayout;
	
	@Override
	public void start(Stage primaryStage) {
		this.primaryStage = primaryStage;
		this.primaryStage.setTitle("MyNotes");
		
		initRootLayout();
		
		showMyNotes();
		
	}

	/**
	 * Initialize primary stage.
	 */
	public void initRootLayout() {
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(Main.class.getResource("notes/view/RootLayout.fxml"));
			rootLayout = loader.load();
			
			Scene scene = new Scene(rootLayout);
			primaryStage.setScene(scene);
			primaryStage.show();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Show my notes in primary stage.
	 */
	public void showMyNotes() {
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(Main.class.getResource("notes/view/ScheduleOverview.fxml"));
			AnchorPane myNotesOverview = (AnchorPane) loader.load();
		
			rootLayout.setCenter(myNotesOverview);
			
			ScheduleOverviewController controller = loader.getController();
			
			// Just test setting of notepad to table 
			controller.addDataToTable(Notepad.getNotepads().get(0).getNotes());
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		// Create test notepad
		Notepad denik = new Notepad("Denik jedna");
		Notepad.addNotepad(denik);
		
		// Add some test notes to notepad
		denik.addNote(Note.createNote("Bakalarka", "Priprav se poradne na bakalarku", 5));
		denik.addNote(Note.createNote("Tralali", "Jen tak si zazpivej", 5));
		
		launch(args);
	}
	/**
	 * @return Primary stage.
	 */
	public Stage getPrimaryStage() {
		return primaryStage;
	}
}
